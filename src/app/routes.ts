import { Routes } from "@angular/router";
import { Error404Component } from "./errors/404.component";
import { CreateEventComponent } from "./events/create-event.component";
import { EventDetailsComponent } from "./events/event-details/event-details.component";
import { EventDetailGuard } from "./events/event-details/event-details.guard";
import { EventListResolver } from "./events/event-list-resolver.service.";
import { EventsListComponent } from "./events/events-list.component";

export const appRoutes:Routes = [
    { path: 'events/new', component: CreateEventComponent },
    { path: 'events', component: EventsListComponent,  resolve: {events: EventListResolver}},
    { path: 'events/:id', component: EventDetailsComponent, canActivate: [EventDetailGuard] },
    { path: '404', component: Error404Component },
    { path: '', redirectTo: '/events', pathMatch: 'full' },
    {
//* Dynamic import returns a promise that contains the module to be imported and we need to return the UserModule class from the import. *//
        path: 'user', 
        loadChildren: () => import('./user/user.module')
        .then(mod => mod.UserModule)
    }
]