import {Component} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
    selector: 'login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent {
    userName!: string;
    password!: string;
    mouseoverLogin!: any;

    constructor(private authService: AuthService, private router: Router) {

    }

    login(formValues: { username: string; password: string; }) {
        this.authService.login(formValues.username, formValues.password);
        this.router.navigate(['events']);
    }

}