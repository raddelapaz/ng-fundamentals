import {NgModule} from '@angular/core';

// Modules 
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//Gives us different types of template-based forms features 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Routes
import { userRoutes } from './user.routes';
import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(userRoutes)
    ],
    declarations: [
        ProfileComponent,
        LoginComponent
    ],
    providers: []
})

export class UserModule {

}