import { Injectable } from '@angular/core';
import { IUser } from './user.model';

@Injectable()
export class AuthService{
  currentUser!: IUser;

  login(userName: string, password: string) {
    this.currentUser = {
      id: 1,
      userName: userName,
      firstName: 'John',
      lastName:  'Titor'
    }
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  updateCurrentUser(firstName: string, lastName: string) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;
  }
    
}