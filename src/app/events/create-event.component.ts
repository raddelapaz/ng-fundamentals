import { Component } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { EventService } from "./shared/event.service";

@Component({
    selector: 'create-event',
    templateUrl: './create-event.component.html'
})

export class CreateEventComponent {
    formValues!: FormGroup;
    isDirty: boolean = true;

    constructor(private router: Router, private eventService: EventService) {

    }

    saveEvent(formValues: any) {
        this.eventService.saveEvent(formValues);
        this.isDirty = false;
        this.router.navigate(['/events']);
    }

    cancel() {
        this.router.navigate(['/events'])
    }

}