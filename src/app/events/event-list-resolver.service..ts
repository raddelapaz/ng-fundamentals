import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Resolve } from '@angular/router'
import { Injectable } from "@angular/core";
import { EventService } from "./shared/event.service";
import { map } from 'rxjs/operators';

@Injectable()
export class EventListResolver implements Resolve<any> {

    /** Preloading data for the components
     * Resolves the page from being blank when the page
     * is loaded but the data wasnt displayed yet.
     */
    constructor(private eventService: EventService, private router: Router) {

    }

    resolve() {
       return this.eventService.getEvents().pipe(map(events => events));
    }
}
