
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EventService } from './shared/event.service';

@Component({
    selector: 'events-list',
    templateUrl: './events-list.component.html'
})

export class EventsListComponent implements OnInit{
  events!: any[]
    //Passing event1 object to the event thumbnail component
    //Inject the Event service in the constructor
    constructor(private eventService: EventService,
      private toastr: ToastrService, private route: ActivatedRoute) {
      
    }

    ngOnInit() {
      this.events = this.route.snapshot.data['events'];
    }

    handleThumbnailClick(eventName: any) {
      this.toastr.success(eventName);
    }

}